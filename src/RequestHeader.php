<?php

namespace KpnApi;

use DateTime;

class RequestHeader {
	public $TransactionId;
	public $Channel;
	public $TenantID;
	public $ProcessTime;
	public $AccessUser;
	private $input;
	
    public function __construct(AccessUser $accessUser) 
    {
		$this->input = time();
		$this->Channel = 10;
		$this->TenantID = 201;
		$this->setTransactionId();
		$this->setProcessTime();
		$this->AccessUser = $accessUser;
    }
	
	private function setTransactionId() {
		$NAMESPACE_UUID = '7036caee6a784d228db0e99cf3957e46';
		$hash = sha1(pack("H*", $NAMESPACE_UUID) . $this->input . random_int(0, 0xffff) . random_int(0, 0xffff));
		$this->TransactionId = sprintf(
				'%08s%04s%04x%04x%12s',
				substr($hash, 0, 8),
				substr($hash, 8, 4),
				(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
				(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
				substr($hash, 20, 12)
		);
	}

	private function setProcessTime() {
		$d = new DateTime();
		$this->ProcessTime = $d->format('Y-m-d\TH:i:s');
	}
	
}
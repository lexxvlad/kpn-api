<?php

namespace KpnApi;

class AccessUser {
	public $AccessUserName;
	public $AccessPassword;
	
    public function __construct($AccessUserName,$AccessPassword) 
    {
		$this->AccessUserName = $AccessUserName;
		$this->AccessPassword = $AccessPassword;
    }
}

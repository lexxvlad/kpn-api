<?php

namespace KpnApi;

use SoapClient;
use Exception;

class KpnApi
{

	private $requestHeader;
	private $client;
	private $testMode = FALSE;

	public function __construct(string $AccessUserName, string $AccessPassword, string $certificate, string $endpoint)
	{
		$accessUser = new AccessUser($AccessUserName, $AccessPassword);
		$this->requestHeader = new RequestHeader($accessUser);
		$this->certificate = $AccessPassword;

		$wsdl = __DIR__ . '/wsdl/HuaweiESP.wsdl';

		$options = array(
			'location'		 => $endpoint,
			'keep_alive'	 => true,
			'trace'			 => true,
			'exceptions'	 => TRUE,
			'local_cert'	 => $certificate,
			'cache_wsdl'	 => WSDL_CACHE_NONE,
			'soap_version'	 => SOAP_1_1,
		);
		$this->client = new SoapClient($wsdl, $options);
	}

	public function setTestMode()
	{
		$this->testMode = TRUE;
	}

	public function simActivate(string $serviceNumber): array
	{
		$remarks = $this->testMode ? 'test simActivate' : 'simActivate';
		$params = $this->createParams($serviceNumber, 1, $remarks);
		return $this->makeRequest('ChangeSubscriberStatusForESP', $params);
	}
	
	public function simDeactivate(string $serviceNumber): array
	{
		$remarks = $this->testMode ? 'test simDeactivate' : 'simDeactivate';
		$params = $this->createParams($serviceNumber, 3, $remarks);
		return $this->makeRequest('ChangeSubscriberStatusForESP', $params);
	}
	
	public function simResume(string $serviceNumber): array
	{
		$remarks = $this->testMode ? 'test simResume' : 'simResume';
		$params = $this->createParams($serviceNumber, 4, $remarks);
		return $this->makeRequest('ChangeSubscriberStatusForESP', $params);
	}
	
	private function makeRequest(string $requestName, array $params):array
	{
		try {
			$data = $this->client->$requestName($params);
			$response = json_decode(json_encode($data), TRUE);
			return $this->returnSuccess($response);
		} catch (Exception $e) {
			return $this->returnError($e->getMessage());
		}
	}

	private function returnSuccess(array $response): array
	{
		return ['request' => 'success', 'error' => '', 'response' => $response];
	}

	private function returnError(string $message): array
	{
		return ['request' => 'error', 'error' => $message, 'response' => []];
	}

	private function createParams(string $serviceNumber, int $operationType, string $remarks = '')
	{
		return [
			'RequestHeader'	 => $this->requestHeader,
			'RequestBody'	 => [
				'IdentifiedBy'	 => [
					'ServiceNumber' => $serviceNumber
				],
				'OperationType'	 => $operationType,
				'Remarks'		 => $remarks
			]
		];
	}

}
